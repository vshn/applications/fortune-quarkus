# Stage 1: build fortune app
FROM ubuntu:20.04 AS fortune
RUN apt-get update
ENV TZ=Europe/Zurich
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get install -y wget cmake build-essential
WORKDIR /build
RUN wget https://github.com/shlomif/fortune-mod/releases/download/fortune-mod-3.6.1/fortune-mod-3.6.1.tar.xz
RUN tar -xf fortune-mod-3.6.1.tar.xz
WORKDIR /build/fortune-mod-3.6.1
RUN mkdir build
WORKDIR /build/fortune-mod-3.6.1/build
RUN cmake ..
RUN make install

# Stage 2: build Quarkus app with maven builder image & native capabilities
FROM quay.io/quarkus/ubi-quarkus-native-image:21.3.0-java17 AS quarkus
COPY --chown=quarkus:quarkus mvnw /code/mvnw
COPY --chown=quarkus:quarkus .mvn /code/.mvn
COPY --chown=quarkus:quarkus pom.xml /code/
USER quarkus
WORKDIR /code
RUN ./mvnw -B org.apache.maven.plugins:maven-dependency-plugin:3.1.2:go-offline
COPY src /code/src
RUN ./mvnw package -Pnative

# tag::production[]
# Stage 3: assemble runtime image
FROM quay.io/quarkus/quarkus-distroless-image:1.0
ENV PATH=/
COPY --from=fortune /usr/local/games/fortune /fortune
COPY --from=fortune /usr/local/bin/strfile /strfile
COPY --from=fortune /usr/local/bin/unstr /unstr
COPY --from=fortune /usr/local/bin/rot /rot
COPY --from=fortune /usr/local/local/share/games/fortunes /usr/local/local/share/games/fortunes
COPY --from=quarkus /code/target/*-runner /application

EXPOSE 8080

# <1>
USER nonroot

CMD ["./application", "-Dquarkus.http.host=0.0.0.0"]
# end::production[]
