package cloud.appuio.fortune;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class FortuneService {
    public String getFortune() {
        try {
            Runtime r = Runtime.getRuntime();
            Process p;
            p = r.exec("fortune");
            p.waitFor();
            BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = "";
            StringBuffer output = new StringBuffer();

            while ((line = b.readLine()) != null) {
                output.append(line);
                output.append("\n");
            }

            b.close();
            return output.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
