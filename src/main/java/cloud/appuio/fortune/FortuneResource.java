package cloud.appuio.fortune;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import io.quarkus.qute.CheckedTemplate;
import io.quarkus.qute.TemplateInstance;

// tag::router[]
@Path("/")
public class FortuneResource {

    @Inject
    FortuneService fortuneService;

    @Inject
    RandomNumberService randomNumberService;

    @Inject
    HostnameService hostnameService;

    @CheckedTemplate
    public static class Templates {
        public static native TemplateInstance index(FortuneData data);
    }

    private FortuneData makeFortuneData() {
        FortuneData data = new FortuneData();
        data.message = fortuneService.getFortune();
        data.number = randomNumberService.generateRandomNumber();
        data.hostname = hostnameService.getHostname();
        data.version = "1.2-java";
        return data;
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.TEXT_HTML)
    public TemplateInstance hello() {
        FortuneData data = makeFortuneData();
        return Templates.index(data);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public FortuneData helloJson() {
        FortuneData data = makeFortuneData();
        return data;
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.TEXT_PLAIN)
    public String helloText() {
        FortuneData data = makeFortuneData();
        return data.toString();
    }
}
// end::router[]
