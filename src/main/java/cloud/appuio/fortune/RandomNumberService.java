package cloud.appuio.fortune;

import java.util.Random;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class RandomNumberService {
    Random random = new Random();

    public int generateRandomNumber() {
        return random.nextInt(0, 1000);
    }
}
